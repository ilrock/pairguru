## Welcome to the completed Pairguru Assignment!


## Things to know:
The API key from the movie DB is set to be an ENV variable called -> **THEMOVIEDBKEY**. Create a .env file in the project root and add:


```
#!ruby

THEMOVIEDBKEY="YOURAPIKEY"
```


## API information:

APIv1

Endpoint: http://localhost:YOURPORT/api/v1/movies -
Will give you the list of all the movies including their id and title along with the status

Endpoint: http://localhost:YOURPORT/api/v1/movies/MOVIEID -
Will give you the movie corresponding to the given id along with the status

APIv2

Endpoint: http://localhost:YOURPORT/api/v2/movies -
Will give you the list of all the movies including their id, title, genre id, genre name and number of movies with the same genre along with the status

Endpoint: http://localhost:YOURPORT/api/v2/movies/MOVIEID -
Will give you the movie corresponding to the given id along with the status
Rails.application.routes.draw do
  devise_for :users

  root 'home#welcome'

  namespace :api do
    namespace :v1 do
      namespace :movies do
        get '/', to: 'movies_api#index'
        get '/:id', to: 'movies_api#show'
      end
    end
    namespace :v2 do
      namespace :movies do
        get '/', to: 'movies_api#index'
        get '/:id', to: 'movies_api#show'
      end
    end
  end

  resources :genres, only: :index do
    member do
      get 'movies'
    end
  end
  resources :movies, only: [:index, :show] do
    member do
      get :send_info
    end
    collection do
      get :export
    end
  end
end

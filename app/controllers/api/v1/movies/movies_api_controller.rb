class Api::V1::Movies::MoviesApiController < ApiController
  def index
    #-- GET ALL THE MOVIES AND MAP THEM INTO AN ARRAY CONTAINING JUST ID AND TITLE
    movies = Movie.all.map {|m| {:id => m.id, :title => m.title} }
    render json: {
      status: :ok,
      results: movies
      }
  end

  def show
    #-- GET THE MOVIE THE USER IS PASSING AS PARAMETER
    if Movie.find_by_id(params[:id])
      result = Movie.find(params[:id])
      status = :ok
    else
      result = "No movie was found with the given id"
      status = :not_found
    end

    render json: {
      status: result,
      results: movie
      }
  end
end
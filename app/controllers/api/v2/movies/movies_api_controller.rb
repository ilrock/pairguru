class Api::V2::Movies::MoviesApiController < ApiController
  def index
    #-- GET ALL THE MOVIES AND EAGERLOAD GENRE
	movies = Movie.includes(:genre)
	#-- MAP THE ARRAY TO GET JUST ID, TITLE, GENRE_ID, GENRE_NAME AND MOVIES-PER-GENRE
	movies = movies.map {|m| {
	  :id => m.id, 
	  :title => m.title, 
	  :genre_id => m.genre_id, 
	  :genre_name => m.genre.name,
	  :movies_by_genre => movies.where(:genre_id => m.genre_id).length
	}}
	render json: {
	  status: :ok,
      results: movies
      }
  end

  def show
    #-- GET THE MOVIE THE USER IS PASSING AS PARAMETER
    if Movie.find_by_id(params[:id])
      result = Movie.find(params[:id])
      status = :ok
    else
      result = "No movie was found with the given id"
      status = :not_found
    end

    render json: {
      status: status,
      results: result
      }
  end
end
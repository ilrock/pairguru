class MoviesController < ApplicationController
  before_action :authenticate_user!, only: [:send_info]

  expose_decorated(:movies) { Movie.all }
  expose(:movie)

  def show
    #-- GET THE IMAGE BASE URL AND SIZE FROM THE GENERAL CONFING OF THE API
    mdb_config = Tmdb::Configuration.get
    mdb_base_url = mdb_config.images.base_url
    mdb_poster_size = mdb_config.images.poster_sizes[1]
    #-- GET THE MOVIE THROUGH THE PARAM
    @movie = Movie.find(params[:id])
    #-- QUERY THE API WITH THE MOVIE NAME AND PULL DESCRIPTION, POSTER AND RATING
    movie_data = Tmdb::Search.movie(@movie.title).results.first
    @description = movie_data.overview
    @poster = mdb_base_url + mdb_poster_size + movie_data.poster_path
    @rating = movie_data.vote_average
    
  end

  def send_info
    MovieInfoMailer.send_info(current_user, movie).deliver_now
    redirect_to :back, notice: 'Email sent with movie info'
  end

  def export
    file_path = 'tmp/movies.csv'
    MovieExporter.new.call(current_user, file_path)
    redirect_to root_path, notice: 'Movies exported'
  end
end
